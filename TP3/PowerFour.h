#ifndef POWERFOUR_H
#define POWERFOUR_H

#include "Game.h"

// Classe du jeu de puissance 4
class PowerFour : public Game
{
public:
	/**
	 * Constructeur
	**/
	PowerFour();
	PowerFour(string player1Name, string player2Name);

	/**
	 * Getters
	**/
	inline Player GetPlayer1() const { return player1; }
	inline Player GetPlayer2() const { return player2; }

	/**
	 * M�thodes
	**/
	void InitGrid(const int& nbLines, const int& nbColumns) override;
	void DisplayGrid() const override;
	void InputPlayer(Player player) override;
	void PlayRound() override;
	bool NobodyWon() const override;
	bool PlayerHasWon(const Player& player) const override;
	bool PlayerHasWonByLine(const Player& player) const override;
	bool PlayerHasWonByColumn(const Player& player) const override;
	bool PlayerHasWonByDiagonal(const Player& player) const override;

private:
	/**
	 * Attributs
	**/
	vector<vector<Box>> grid;
	Player player1;
	Player player2;
};

#endif