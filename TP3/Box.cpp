#include "Box.h"

int Box::idStatic = 1;

/**
 * @brief   Construit une nouvelle case
**/
Box::Box() {
	owner = 0;
	idBox = Box::idStatic++;
}

/**
 * @brief   Renvoie l'affichage d'une case pour le jeu de morpion
 * @return  X pour le joueur 1, O pour le joueur 2, idBox sinon
**/
string Box::DisplayBoxTicTacToe() const
{
	switch (owner)
	{
	case 1:
		return "X";
	case 2:
		return "O";
	default:
		return to_string(idBox);
	}
}

/**
 * @brief   Renvoie l'affichage d'une case pour le jeu de puissance 4
 * @return  E pour le joueur 1, O pour le joueur 2, cha�ne vide sinon
**/
string Box::DisplayBoxPowerFour() const
{
	switch (owner)
	{
	case 1:
		return "E";
	case 2:
		return "O";
	default:
		return " ";
	}
}