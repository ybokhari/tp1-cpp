#include <iostream>
#include <math.h>
#include "Point.h"
#include "Rectangle.h"
#include "Cercle.h"
#include "Triangle.h"

using namespace std;

int main()
{
    /*********************** POINT ***********************/
    Point pointQuelconque;
    pointQuelconque.setX(6);
    pointQuelconque.setY(5);
    pointQuelconque.Afficher();

    /*********************** RECTANGLE ***********************/
    Rectangle ABCD(7, 5);
    Rectangle rectangleParam(2, 2);
    ABCD.Afficher(rectangleParam);

    /*********************** CERCLE ***********************/
    Cercle C(6, 4, 5);
    C.Afficher(pointQuelconque);

    /*********************** TRIANGLE ***********************/
    Point pointA;
    pointA.setX(0);
    pointA.setY(1);
    Point pointB;
    pointB.setX(sqrt(3)/2);
    pointB.setY(-0.5);
    Point pointC;
    pointC.setX(- sqrt(3) / 2);
    pointC.setY(-0.5);
    Triangle ABC(pointA, pointB, pointC);
    ABC.Afficher();

    return 0;
}