#include <iostream>
#include <string>
#include <cctype>

#include "tennis.hpp"

// II
void calculerScore(int &scoreJoueurGagnant, int &scoreJoueurPerdant)
{
    switch (scoreJoueurGagnant)
    {
    case 0:
    case 15:
        scoreJoueurGagnant += 15;
        break;
    case 30:
        scoreJoueurGagnant += 10;
        break;
    case 40:
        // si le joueur perdant a l'avantage, il revient à égalité
        if (scoreJoueurPerdant == 99)
        {
            scoreJoueurPerdant = 40;
        }
        // si le joueur perdant a 40 points, le gagnant prend l'avantage
        else if (scoreJoueurPerdant == 40)
        {
            scoreJoueurGagnant = 99;
        }
        // sinon le joueur gagnant remporte le jeu
        else
        {
            scoreJoueurGagnant = 100;
        }
        break;
    case 99:
        // le joueur gagnant remporte le jeu
        scoreJoueurGagnant = 100;
        break;
    }
}

void jouerAuTennis()
{
    bool jeuTermine = false;
    int scoreJoueurA = 0;
    int scoreJoueurB = 0;
    int i = 1;

    while (!jeuTermine)
    {
        std::cout << "-------------------------------------------------------------------" << std::endl;
        std::cout << "Qui a gagné l'échange n°" << i << " ? ";
        std::string joueur;
        std::cin >> joueur;
        joueur[0] = toupper(joueur[0]);

        if (joueur == "A")
        {
            calculerScore(scoreJoueurA, scoreJoueurB);
        }
        else if (joueur == "B")
        {
            calculerScore(scoreJoueurB, scoreJoueurA);
        }
        else
        {
            std::cout << "Erreur : merci de me dire si le joueur A ou B a gagné." << std::endl;
            continue;
        }

        // si l'un des joueurs a remporté la victoire, le jeu est terminé
        if (scoreJoueurA == 100 || scoreJoueurB == 100)
        {
            std::cout << "Le joueur " << joueur << " a remporté le jeu ! Bravo !" << std::endl;
            jeuTermine = true;
        }
        // sinon on continue d'afficher le score du jeu
        else
        {
            std::cout << "Joueur A : " << (scoreJoueurA == 99 ? "Avantage" : std::to_string(scoreJoueurA));
            std::cout << " - Joueur B : " << (scoreJoueurB == 99 ? "Avantage" : std::to_string(scoreJoueurB)) << std::endl;
            i++;
        }
    }
}