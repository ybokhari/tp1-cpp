#ifndef POINT_H
#define POINT_H

struct Point
{
public:
	/*********************** SETTERS ***********************/
	void setX(float xParam);
	void setY(float yParam);

	/*********************** GETTERS ***********************/
	float getX();
	float getY();

	/*********************** M�THODES ***********************/
	void Afficher();

private :
	// Membres
	float x;
	float y;
};

#endif // POINT_H