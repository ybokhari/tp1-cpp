#ifndef INSCRIPTION_RECUP_H
#define INSCRIPTION_RECUP_H

// III.1.1
void demanderPrenom();

// III.1.2
void demanderPrenomNom();

// III.2.1 & 2.2
void devinerNombre();
void afficherMessageVictoire(int nombreMystere, int nombreEssais);

// III.2.3
void devinerNombreOrdinateur();

#endif // INSCRIPTION_RECUP_H