#define _USE_MATH_DEFINES

#include <iostream>
#include <math.h>
#include "Cercle.h"

using namespace std;


/*********************** CONSTRUCTEUR ***********************/
/**
 * @brief   Construire un nouveau cercle
 * @param   diametre : diam�tre du cercle
 * @params  x, y : coordonn�es du centre du cercle
**/
Cercle::Cercle(int diametre, float x, float y)
{
	setDiametre(diametre);
	centre.setX(x);
	centre.setY(y);
}

/*********************** SETTERS ***********************/
void Cercle::setDiametre(int diametreParam)
{
	diametre = diametreParam;
}

/*********************** GETTERS ***********************/
int Cercle::getDiametre()
{
	return diametre;
}

/*********************** M�THODES ***********************/
/**
 * @brief   Affiche les informations de l'objet
 * @param   pointQuelconque : point requis pour les m�thodes EstSurLeCercle() et EstDansLeCercle()
**/
void Cercle::Afficher(Point pointQuelconque)
{
	cout << "-------------------------- CERCLE --------------------------" << endl;
	cout << "Perimetre = " << Perimetre() << endl;
	cout << "Surface = " << Surface() << endl;
	cout << "Point p est sur le cercle ? " << (EstSurLeCercle(pointQuelconque) ? "Oui" : "Non") << endl;
	cout << "Point p est dans le cercle ? " << (EstDansLeCercle(pointQuelconque) ? "Oui" : "Non") << endl;
}

/**
 * @brief   Calcule le p�rim�tre du cercle
 * @return  float
**/
float Cercle::Perimetre()
{
	return static_cast<float>(diametre) * M_PI;
}

/**
 * @brief   Calcule la surface du cercle
 * @return  float
**/
float Cercle::Surface()
{
	return M_PI * pow((static_cast<float>(diametre) / 2), 2);
}

/**
 * @brief   V�rifie si un point est sur le cercle d'apr�s l'�quation cart�sienne du cercle
 * @param   point : point � v�rifier
 * @return  bool
**/
bool Cercle::EstSurLeCercle(Point pointQuelconque)
{
	return pow(pointQuelconque.getX() - centre.getX(), 2) + pow(pointQuelconque.getY() - centre.getY(), 2) == static_cast<float>(getDiametre());
}

/**
 * @brief   V�rifie si un point est dans le cercle d'apr�s l'�quation cart�sienne du cercle
 * @param   point : point � v�rifier
 * @return  bool
**/
bool Cercle::EstDansLeCercle(Point pointQuelconque)
{
	return pow(pointQuelconque.getX() - centre.getX(), 2) + pow(pointQuelconque.getY() - centre.getY(), 2) < static_cast<float>(getDiametre());
}



