#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <vector>

using namespace std;

// Classe du joueur
class Player
{
public:
	/**
	 * Constructeur
	**/
	Player();
	Player(int id, string name);

	/**
	 * Getters
	**/
	inline int GetId() const { return id; }
	inline string GetName() const { return name; }

	/**
	 * Setters
	**/
	inline void SetName(const string& NewName) { name = NewName; }

private:
	/**
	 * Attributs
	**/
	int id;
	string name;
};

#endif