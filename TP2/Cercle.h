#ifndef CERCLE_H
#define CERCLE_H

#pragma once
#include "Point.h"

class Cercle
{
public:
    /*********************** CONSTRUCTEUR ***********************/
    /**
     * @brief   Construire un nouveau cercle
     * @param   diametre : diam�tre du cercle
     * @params  x, y : coordonn�es du centre du cercle
    **/
    Cercle(int diametre, float x, float y);

    /*********************** SETTERS ***********************/
    void setDiametre(int diametreParam);

    /*********************** GETTERS ***********************/
    int getDiametre();

    /*********************** M�THODES ***********************/
    /**
     * @brief   Affiche les informations de l'objet
     * @param   pointQuelconque : point requis pour les m�thodes EstSurLeCercle() et EstDansLeCercle()
    **/
    void Afficher(Point pointQuelconque);

    /**
     * @brief   Calcule le p�rim�tre d'un cercle
     * @return  float
    **/
    float Perimetre();

    /**
     * @brief   Calcule la surface d'un cercle
     * @return  float
    **/
    float Surface();

    /**
     * @brief   V�rifie si un point est sur le cercle d'apr�s l'�quation cart�sienne du cercle
     * @param   pointQuelconque : point � v�rifier
     * @return  bool
    **/
    bool EstSurLeCercle(Point pointQuelconque);

    /**
     * @brief   V�rifie si un point est dans le cercle d'apr�s l'�quation cart�sienne du cercle
     * @param   pointQuelconque : point � v�rifier
     * @return  bool
    **/
    bool EstDansLeCercle(Point pointQuelconque);

private:
    /*********************** MEMBRES ***********************/
    int diametre;
    Point centre;
};

#endif // CERCLE_H


