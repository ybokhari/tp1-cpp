#include <iostream>
#include "Game.h"
#include "TicTacToe.h"
#include "PowerFour.h"

using namespace std;

int main()
{
    cout << "Bienvenue dans ce magnifique programme ! Que souhaitez-vous faire ?" << endl;
    cout << "1 : Jouer au morpion" << endl;
    cout << "2 : Jouer au puissance 4" << endl;
    cout << "3 : Quitter le programme" << endl;
    cout << "Choix : ";
    int choix;
    cin >> choix;

    // v�rifier si le choix est valide (nombre compris entre 1 et 3)
    while (!std::cin.good() || choix < 1 || choix > 3)
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        cout << "Veuillez saisir un nombre entre 1 et 3 : ";
        cin >> choix;
    }

    Game* game;
    switch (choix)
    {
    case 1:
        game = new TicTacToe();
        break;
    case 2:
        game = new PowerFour();
        break;
    default:
        return 0;
    }

    game->PlayRound();
    delete game;

    return 0;
}