#ifndef RECTANGLE_H
#define RECTANGLE_H

#pragma once
#include "Point.h"

class Rectangle
{
public:
    /*********************** CONSTRUCTEUR ***********************/
    /**
     * @brief   Construire un nouveau rectangle
     * @param   longueur : longueur du rectangle
     * @param   largeur : largeur du rectangle
    **/
    Rectangle(int longueur, int largeur);

    /*********************** SETTERS ***********************/
    void setLongueur(int longueur);
    void setLargeur(int largeur);

    /*********************** GETTERS ***********************/
    int getLongueur();
    int getLargeur();

    /*********************** M�THODES ***********************/
    /**
     * @brief   Affiche les informations de l'objet
     * @param   rectangleParam : recatngle requis pour les m�thodes PerimetreObjEstPlusGrand() et SurfaceObjEstPlusGrand()
    **/
    void Afficher(Rectangle rectangleParam);

    /**
     * @brief   Calcule le p�rim�tre du rectangle
     * @return  int
    **/
    int Perimetre();

    /**
     * @brief   Calcule la surface du rectangle
     * @return  int
    **/
    int Surface();

    /**
     * @brief   V�rifie si le p�rim�tre de l'objet est plus grand que celui du rectangle pass� en param�tre
     * @param   rectangleParam : rectangle � comparer
     * @return  bool
    **/
    bool PerimetreObjEstPlusGrand(Rectangle rectangleParam);

    /**
     * @brief   V�rifie si la surface de l'objet est plus grande que celle du rectangle pass� en param�tre
     * @param   rectangleParam : rectangle � comparer
     * @return  bool
    **/
    bool SurfaceObjEstPlusGrand(Rectangle rectangleParam);

private:
    /*********************** MEMBRES ***********************/
    int longueur;
    int largeur;
    Point coinSuperieurGauche;
};

#endif // RECTANGLE_H

