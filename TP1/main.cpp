#include <iostream>

#include "manip_nombres.hpp"
#include "tennis.hpp"
#include "inscription_recup.hpp"

int main()
{
    std::cout << "Bienvenue dans ce programme." << std::endl;

    std::cout << "Quelle partie souhaites-tu exécuter ? " << std::endl;
    std::cout << "1 : Manipulation de nombres" << std::endl;
    std::cout << "2 : Jeu de tennis" << std::endl;
    std::cout << "3 : Inscription dans la console et récupération des saisies" << std::endl;
    std::cout << "Partie souhaitée : ";
    int choix;
    std::cin >> choix;

    // gestion des erreurs
    while (std::cin.fail() || choix < 1 || choix > 3)
    {
        std::cout << "Erreur : partie non valide." << std::endl;
        std::cout << "Partie souhaitée : ";
        std::cin.clear();
        std::cin.ignore(256, '\n');
        std::cin >> choix;
    }

    switch (choix)
    {
    case 1:
        std::cout << "Dans cette partie nous allons générer un tableau d’entiers rempli de valeurs aléatoires." << std::endl;
        std::cout << "Donne-moi la taille du tableau : ";
        int taille;
        std::cin >> taille;

        // gestion des erreurs
        while (std::cin.fail() || taille <= 0)
        {
            std::cout << "Erreur : taille non valide." << std::endl;
            std::cout << "Donne-moi la taille du tableau : ";
            std::cin.clear();
            std::cin.ignore(256, '\n');
            std::cin >> taille;
        }

        std::cout << "Donne-moi le type de tri (1 = croissant, 2 = décroissant, 3 = inverse) : ";
        int tri;
        std::cin >> tri;

        // gestion des erreurs
        while (std::cin.fail() || tri < 1 || tri > 3)
        {
            std::cout << "Erreur : tri non valide." << std::endl;
            std::cout << "Donne-moi le type de tri (1 = croissant, 2 = décroissant, 3 = inverse) : ";
            std::cin.clear();
            std::cin.ignore(256, '\n');
            std::cin >> tri;
        }

        GenererTableau(taille, tri);
        break;
    case 2:
        std::cout << "Dans cette partie nous allons jouer au tennis. À chaque échange, dis-moi quel joueur a gagné (A ou B) et je te donnerai le score du jeu." << std::endl;
        std::cout << "Que le meilleur gagne !" << std::endl;
        std::cout << "Joueur A : 0 - Joueur B : 0" << std::endl;
        jouerAuTennis();
        break;
    case 3:
        std::cout << "Dans cette partie nous allons jouer à une petite devinette. Pense à un nombre entre 0 et 1000 et je vais tenter de le trouver !" << std::endl;
        std::cout << "Si le nombre est plus grand, dis-moi '+'. S'il est plus petit, dis-moi '-'. Sinon c'est que j'ai trouvé le bon nombre, dis-moi donc 'Y' !" << std::endl;
        devinerNombreOrdinateur();
        break;
    }

    return 0;
}