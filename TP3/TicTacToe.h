#ifndef TICTACTOE_H
#define TICTACTOE_H

#include "Game.h"

// Classe du jeu de morpion
class TicTacToe : public Game
{
public:
	/**
	 * Constructeur
	**/
	TicTacToe();
	TicTacToe(string player1Name, string player2Name);

	/**
	 * Getters
	**/
	inline Player GetPlayer1() const { return player1; }
	inline Player GetPlayer2() const { return player2; }

	/**
	 * M�thodes
	**/
	Box& GetBoxPosition(const int& idBox);
	void InitGrid(const int& nbLines, const int& nbColumns) override;
	void DisplayGrid() const override;
	void InputPlayer(Player player) override;
	void PlayRound() override;
	bool NobodyWon() const override;
	bool PlayerHasWon(const Player& player) const override;
	bool PlayerHasWonByLine(const Player& player) const override;
	bool PlayerHasWonByColumn(const Player& player) const override;
	bool PlayerHasWonByDiagonal(const Player& player) const override;

private:
	/**
	 * Attributs
	**/
	vector<vector<Box>> grid;
	Player player1;
	Player player2;
};

#endif