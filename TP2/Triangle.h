#ifndef TRIANGLE_H
#define TRIANGLE_H

#pragma once
#include "Point.h"

class Triangle
{
public:
    /*********************** CONSTRUCTEUR ***********************/
    /**
     * @brief   Construire un nouveau triangle
     * @params	a, b, c : trois points du triangle
    **/
    Triangle(Point a, Point b, Point c);

    /*********************** SETTERS ***********************/
    void setA(Point aParam);
    void setB(Point bParam);
    void setC(Point cParam);

    /*********************** GETTERS ***********************/
    Point getA();
    Point getB();
    Point getC();

    /*********************** M�THODES ***********************/
    /**
     * @brief   Affiche les informations de l'objet
    **/
    void Afficher();

    /**
     * @brief   Calcule la distance entre deux points
     * @return  float
    **/
    float Distance(Point p1, Point p2);

    /**
     * @brief   Retourne la base du triangle
     * @return  float
    **/
    float Base();

    /**
     * @brief   Calcule le p�rim�tre du triangle
     * @return  float
    **/
    float Perimetre();

    /**
     * @brief   Calcule la surface du triangle d'apr�s la formule de H�ron
     * @return  float
    **/
    float Surface();

    /**
     * @brief   Calcule la hauteur du triangle d'apr�s la formule du calcul de la surface
     * @return  float
    **/
    float Hauteur();

    /**
     * @brief   V�rifie si le triangle est isoc�le
     * @return  bool
    **/
    bool EstIsocele();

    /**
     * @brief   V�rifie si le triangle est �quilat�ral
     * @return  bool
    **/
    bool EstEquilateral();

    /**
     * @brief   V�rifie si le triangle est rectangle d'apr�s la formule de Pythagore
     * @return  bool
    **/
    bool EstRectangle();

private:
    /*********************** MEMBRES ***********************/
    Point a;
    Point b;
    Point c;
};

#endif // TRIANGLE_H

