#include "PowerFour.h"

/**
 * @brief   Construit un nouveau jeu de puissance 4
**/
PowerFour::PowerFour() {
	InitGrid(4, 7);
	player1 = Player(1, "1");
	player2 = Player(2, "2");
}

/**
 * @brief   Initialise une grille de jeu
 * @params  nbLines : nombre de lignes, nbColumns : nombre de colonnes
 * @return  void
**/
void PowerFour::InitGrid(const int& nbLines, const int& nbColumns)
{
	grid.resize(nbLines);
	for (int i = 0; i < nbLines; i++) {
		grid[i].resize(nbColumns);
	}
	Box::SetIdStatic(1);
}

/**
 * @brief   Affiche une grille de jeu
 * @return  void
**/
void PowerFour::DisplayGrid() const
{
	cout << "Joueur 1 (E) - Joueur 2 (O)" << endl << endl;

	string header = "";
	for (int i = 1; i <= 7; i++)
	{
		if (i == 1)
		{
			header += "|  ";
		}
		header += std::to_string(i);
		if (i != 7)
		{
			header += "  |  ";
		}
		else
		{
			header += "  |";
		}
	}
	cout << header << endl;
	cout << "|     |     |     |     |     |     |     |" << endl;

	string line;
	for (int i = 3; i >= 0; i--)
	{
		line = "|  ";
		for (int j = 0; j < 7; j++)
		{
			line += grid[i][j].DisplayBoxPowerFour();
			if (j != 7)
			{
				line += "  |  ";
			}
			else
			{
				line += "  |";
			}
		}
		cout << line << endl;
		if (i > 0)
		{
			cout << "|     |     |     |     |     |     |     |" << endl;
		}
	}

	cout << endl;
}

/**
 * @brief   Demande une entr�e au joueur
 * @param   player : joueur
 * @return  void
**/
void PowerFour::InputPlayer(Player player) {

	int input = 0;
	cin >> input;

	// v�rifier si l'entr�e est valide (nombre compris entre 1 et 7)
	while (!std::cin.good() || input < 1 || input > 7)
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << "Veuillez saisir un nombre entre 1 et 7 : ";
		cin >> input;
	}

	// v�rifier si la colonne est libre
	for (int line = 0; line < grid.size(); line++) {

		if (grid[line][input - 1].GetOwner() == 0)
		{
			grid[line][input - 1].SetOwner(player.GetId());
			break;
		}
		else if (line == grid.size() - 1)
		{
			cout << "La colonne est pleine ! Veuillez choisir une autre colonne : ";
			InputPlayer(player);
		}
	}
}

/**
 * @brief   Lance un tour de jeu
 * @return  void
**/
void PowerFour::PlayRound()
{
	int i = 0;
	Player currentPlayer = player1;

	while (!PlayerHasWon(currentPlayer) && !NobodyWon())
	{
		if (i % 2 == 0)
		{
			currentPlayer = player1;
		}
		else
		{
			currentPlayer = player2;
		}

		system("cls");
		DisplayGrid();
		cout << "Au tour du joueur " << currentPlayer.GetName() << ", dans quelle colonne souhaitez-vous jouer ? ";
		InputPlayer(currentPlayer);
		i++;
	}

	system("cls");
	DisplayGrid();

	if (PlayerHasWon(currentPlayer))
	{
		cout << "Victoire pour le joueur " << currentPlayer.GetName() << " !" << endl;
	}
	else if (NobodyWon())
	{
		cout << "Aucun joueur n'a remporte la victoire" << endl;
	}
}

/**
 * @brief   V�rifie si aucun joueur n'a gagn� � l'issue du jeu
 * @return  true si aucun joueur n'a gagn�, false sinon
**/
bool PowerFour::NobodyWon() const
{
	int line = 0;
	int column = 0;
	for (line = 0; line < grid.size(); line++)
	{
		for (column = 0; column < grid[line].size(); column++)
		{
			if (grid[line][column].GetOwner() == 0)
			{
				return false;
			}
		}
	}
	return true;
}

/**
 * @brief   V�rifie si un joueur a gagn�
 * @param   player : joueur
 * @return  true si le joueur a gagn�, false sinon
**/
bool PowerFour::PlayerHasWon(const Player& player) const
{
	return PlayerHasWonByLine(player) || PlayerHasWonByColumn(player) || PlayerHasWonByDiagonal(player);
}

/**
 * @brief   V�rifie si un joueur a gagn� en ligne
 * @param   player : joueur
 * @return  true si le joueur a gagn� en ligne, false sinon
**/
bool PowerFour::PlayerHasWonByLine(const Player& player) const
{
	int count = 0;
	for (int line = 0; line < grid.size(); line++)
	{
		for (int column = 0; column < grid[line].size(); column++)
		{
			if (grid[line][column].GetOwner() == player.GetId())
			{
				count++;

				if (count == 4)
				{
					return true;
				}
			}
			else
			{
				count = 0;
			}
		}
		count = 0;
	}
	return false;
}

/**
 * @brief   V�rifie si un joueur a gagn� en colonne
 * @param   player : joueur
 * @return  true si le joueur a gagn� en colonne, false sinon
**/
bool PowerFour::PlayerHasWonByColumn(const Player& player) const
{
	int count = 0;
	for (int column = 0; column < grid[0].size(); column++) {

		for (int line = 0; line < grid.size(); line++) {

			if (grid[line][column].GetOwner() == player.GetId())
			{
				count++;

				if (count == 4)
				{
					return true;
				}
			}
			else
			{
				count = 0;
			}
		}
		count = 0;
	}
	return false;
}

/**
 * @brief   V�rifie si un joueur a gagn� en diagonale
 * @param   player : joueur
 * @return  true si le joueur a gagn� en diagonale, false sinon
**/
bool PowerFour::PlayerHasWonByDiagonal(const Player& player) const
{
	const int gridColumnNumber = grid[0].size() - 1;
	int line = 0;
	int totalCount = 0;
	int countDiagonal = 0;

	for (int column = 0; column < 4; column++)
	{
		if (grid[line][column].GetOwner() == player.GetId())
		{
			for (countDiagonal = 0; countDiagonal < 4; countDiagonal++)
			{
				if (grid[line + countDiagonal][column + countDiagonal].GetOwner() == player.GetId())
				{
					totalCount++;
				}
				else
				{
					totalCount = 0;
				}
			}
			if (totalCount == 4)
			{
				return true;
			}
			else
			{
				totalCount = 0;
			}

			for (countDiagonal = 0; countDiagonal < 4; countDiagonal++)
			{
				if (grid[line + countDiagonal][gridColumnNumber - column - countDiagonal].GetOwner() == player.GetId())
				{
					totalCount++;
				}
				else
				{
					totalCount = 0;
				}
			}
			if (totalCount == 4)
			{
				return true;
			}
			else
			{
				totalCount = 0;
			}
		}
	}
	return false;
}