#include <iostream>
#include <string>
#include <algorithm>
#include <time.h>
#include <math.h>

#include "manip_nombres.hpp"

// I.1.1
int Somme(int a, int b)
{
    return a + b;
}

// I.1.2
void Inverser(int &a, int &b)
{
    int c = a;
    a = b;
    b = c;
}

// I.1.3
void RemplacerParam3AvecPnt(int a, int b, int *c)
{
    (*c) = Somme(a, b);
}

void RemplacerParam3AvecRef(int d, int e, int &f)
{
    f = Somme(d, e);
}

// I.1.4
void GenererTableau(int taille, int tri)
{
    int *tab = new int(taille);
    srand(time(0));

    if (tri == 1 || tri == 2 || tri == 3)
    {
        std::cout << "Tableau avant tri :" << std::endl;

        // construction du tableau
        for (int i = 0; i < taille; i++)
        {
            tab[i] = rand();
            std::cout << tab[i] << std::endl;
        }

        std::cout << "--------------------------------" << std::endl;
        std::cout << "Tableau après tri :" << std::endl;
    }

    switch (tri)
    {
    // tri croissant (1) ou décroissant (2)
    case 1:
    case 2:
        for (int i = taille - 1; i >= 1; i--)
        {
            bool tabTrie = true;

            for (int j = 0; j <= i - 1; j++)
            {
                if ((tri == 1 && tab[j + 1] < tab[j]) || (tri == 2 && tab[j + 1] > tab[j]))
                {
                    Inverser(tab[j], tab[j + 1]);
                    tabTrie = false;
                }
            }

            if (tabTrie)
            {
                break;
            }
        }
        break;
    // tri inverse (3)
    case 3:
        for (int i = 0; i < floor(taille / 2); i++)
        {
            Inverser(tab[i], tab[taille - 1 - i]);
        }
        break;
    }

    if (tri == 1 || tri == 2 || tri == 3)
    {
        for (int i = 0; i < taille; i++)
        {
            std::cout << tab[i] << std::endl;
        }
    }
}