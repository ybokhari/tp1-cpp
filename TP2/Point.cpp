#include <iostream>
#include "Point.h"

using namespace std;

/*********************** SETTERS ***********************/
void Point::setX(float xParam)
{
	x = xParam;
}
void Point::setY(float yParam)
{
	y = yParam;
}

/*********************** GETTERS ***********************/
float Point::getX()
{
	return x;
}
float Point::getY()
{
	return y;
}

/*********************** M�THODES ***********************/
/**
 * @brief   Affiche les informations de l'objet
**/
void Point::Afficher()
{
	cout << "-------------------------- POINT --------------------------" << endl;
	cout << "x = " << getX() << endl;
	cout << "y = " << getY() << endl;
}
