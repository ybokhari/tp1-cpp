#include <iostream>
#include "Rectangle.h"

using namespace std;

/*********************** CONSTRUCTEUR ***********************/
/**
 * @brief   Construire un nouveau rectangle
 * @param   longueur : longueur du rectangle
 * @param   largeur : largeur du rectangle
**/
Rectangle::Rectangle(int longueur, int largeur)
{
	setLongueur(longueur);
	setLargeur(largeur);
	coinSuperieurGauche.setX(0.0);
	coinSuperieurGauche.setY(largeur);
}

/*********************** SETTERS ***********************/
void Rectangle::setLongueur(int longueurParam)
{
	longueur = longueurParam;
}
void Rectangle::setLargeur(int largeurParam)
{
	largeur = largeurParam;
}

/*********************** GETTERS ***********************/
int Rectangle::getLongueur()
{
	return longueur;
}
int Rectangle::getLargeur()
{
	return largeur;
}

/*********************** M�THODES ***********************/
/**
 * @brief   Affiche les informations de l'objet
 * @param   rectangleParam : recatngle requis pour les m�thodes PerimetreObjEstPlusGrand() et SurfaceObjEstPlusGrand()
**/
void Rectangle::Afficher(Rectangle rectangleParam)
{
	cout << "-------------------------- RECTANGLE --------------------------" << endl;
	cout << "Perimetre = " << Perimetre() << endl;
	cout << "Surface = " << Surface() << endl;
	cout << "Perimetre de l'objet est-il plus grand que celui du rectangle passe en parametre ? : " << (PerimetreObjEstPlusGrand(rectangleParam) ? "Oui" : "Non") << endl;
	cout << "Surface de l'objet est-elle plus grande que celle du rectangle passe en parametre ? : " << (SurfaceObjEstPlusGrand(rectangleParam) ? "Oui" : "Non") << endl;
}

/**
 * @brief   Calcule le p�rim�tre du rectangle
 * @return  int
**/
int Rectangle::Perimetre()
{
	return largeur * 2 + longueur * 2;
}

/**
 * @brief   Calcule la surface du rectangle
 * @return  int
**/
int Rectangle::Surface()
{
	return largeur * longueur;
}

/**
 * @brief   V�rifie si le p�rim�tre de l'objet est plus grand que celui du rectangle pass� en param�tre
 * @param   rectangleParam : rectangle � comparer
 * @return  bool
**/
bool Rectangle::PerimetreObjEstPlusGrand(Rectangle rectangleParam)
{
	return Rectangle::Perimetre() > rectangleParam.Perimetre();
}

/**
 * @brief   V�rifie si la surface de l'objet est plus grande que celle du rectangle pass� en param�tre
 * @param   rectangleParam : rectangle � comparer
 * @return  bool
**/
bool Rectangle::SurfaceObjEstPlusGrand(Rectangle rectangleParam)
{
	return Rectangle::Surface() > rectangleParam.Surface();
}