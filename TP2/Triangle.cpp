#include <iostream>
#include <math.h>
#include "Triangle.h"

using namespace std;

/*********************** CONSTRUCTEUR ***********************/
/**
 * @brief   Construire un nouveau triangle
 * @params	a, b, c : trois points du triangle
**/
Triangle::Triangle(Point a, Point b, Point c)
{
	setA(a);
	setB(b);
	setC(c);
}


/*********************** SETTERS ***********************/
void Triangle::setA(Point aParam)
{
	a = aParam;
}
void Triangle::setB(Point bParam)
{
	b = bParam;
}
void Triangle::setC(Point cParam)
{
	c = cParam;
}

/*********************** GETTERS ***********************/
Point Triangle::getA()
{
	return a;
}
Point Triangle::getB()
{
	return b;
}
Point Triangle::getC()
{
	return c;
}

/*********************** M�THODES ***********************/
/**
 * @brief   Affiche les informations de l'objet
**/
void Triangle::Afficher()
{
	cout << "-------------------------- TRIANGLE --------------------------" << endl;
	cout << "Longueur AB = " << Distance(getA(), getB()) << endl;
	cout << "Longueur AC = " << Distance(getA(), getC()) << endl;
	cout << "Longueur BC = " << Distance(getB(), getC()) << endl;
	cout << "Base = " << Base() << endl;
	cout << "Perimetre = " << Perimetre() << endl;
	cout << "Surface = " << Surface() << endl;
	cout << "Hauteur = " << Hauteur() << endl;
	cout << "Triangle isocele ? " << (EstIsocele() ? "Oui" : "Non") << endl;
	cout << "Triangle equilateral ? " << (EstEquilateral() ? "Oui" : "Non") << endl;
	cout << "Triangle rectangle ? " << (EstRectangle() ? "Oui" : "Non") << endl;
}

/**
 * @brief   Calcule la distance entre deux points
 * @return  float
**/
float Triangle::Distance(Point p1, Point p2)
{
	return sqrt(pow(p2.getX() - p1.getX(), 2) + pow(p2.getY() - p1.getY(), 2));
}

/**
 * @brief   Retourne la base du triangle
 * @return  float
**/
float Triangle::Base()
{
	float base;
	float AB = Distance(a, b);
	float AC = Distance(a, c);
	float BC = Distance(b, c);

	// AB avec symbole sup�rieur ou �gal car si AB == AC == BC, on retourne directement AB
	if (AB >= AC && AB >= BC)
	{
		base = AB;
	}
	else if (AC > AB && AC > BC)
	{
		base = AC;
	}
	else
	{
		base = BC;
	}

	return base;
}

/**
 * @brief   Calcule le p�rim�tre du triangle
 * @return  float
**/
float Triangle::Perimetre()
{
	return Distance(a, b) + Distance(a, c) + Distance(b, c);
}

/**
 * @brief   Calcule la surface du triangle d'apr�s la formule de H�ron
 * @return  float
**/
float Triangle::Surface()
{
	float AB = Distance(a, b);
	float AC = Distance(a, c);
	float BC = Distance(b, c);
	float demiPerimetre = Perimetre() / 2;

	return sqrt(demiPerimetre * (demiPerimetre - AB) * (demiPerimetre - AC) * (demiPerimetre - BC));
}

/**
 * @brief   Calcule la hauteur du triangle d'apr�s la formule du calcul de la surface
 * @return  float
**/
float Triangle::Hauteur()
{
	return (Surface() * 2) / Base();
}

/**
 * @brief   V�rifie si le triangle est isoc�le
 * @return  bool
**/
bool Triangle::EstIsocele()
{
	float AB = Distance(a, b);
	float AC = Distance(a, c);
	float BC = Distance(b, c);

	return AB == AC || AB == BC || AC == BC;
}

/**
 * @brief   V�rifie si le triangle est �quilat�ral
 * @return  bool
**/
bool Triangle::EstEquilateral()
{
	float AB = Distance(a, b);
	float AC = Distance(a, c);
	float BC = Distance(b, c);

	return AB == AC && AC == BC;
}

/**
 * @brief   V�rifie si le triangle est rectangle d'apr�s la formule de Pythagore
 * @return  bool
**/
bool Triangle::EstRectangle()
{
	float AB = Distance(a, b);
	float AC = Distance(a, c);
	float BC = Distance(b, c);

	return pow(AB, 2) == pow(AC, 2) + pow(BC, 2)
		|| pow(AC, 2) == pow(AB, 2) + pow(BC, 2)
		|| pow(BC, 2) == pow(AB, 2) + pow(AC, 2);
}
