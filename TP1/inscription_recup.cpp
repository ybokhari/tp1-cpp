#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <functional>

#include "inscription_recup.hpp"

// III.1.1
void demanderPrenom()
{
    std::cout << "Donne-moi ton prénom : ";
    std::string prenom;
    std::cin >> prenom;

    // gestion des erreurs
    while (std::cin.fail() || std::find_if(prenom.begin(), prenom.end(), std::not1(std::ptr_fun((int (*)(int))std::isalpha))) != prenom.end())
    {
        std::cout << "Erreur : merci de saisir des caractères alphabétiques pour le prénom." << std::endl;
        std::cout << "Donne-moi ton prénom : ";
        std::cin >> prenom;
    }

    std::cout << "Salut " << prenom << " !" << std::endl;
}

// III.1.2
void demanderPrenomNom()
{
    std::cout << "Donne-moi ton prénom et ton nom : ";
    std::string prenomNom;
    std::getline(std::cin, prenomNom);

    // gestion des erreurs
    while (std::cin.fail() || std::find_if(prenomNom.begin(), prenomNom.end(), std::not1(std::ptr_fun((int (*)(int))std::isalpha))) != prenomNom.end())
    {
        std::cout << "Erreur : merci de saisir des caractères alphabétiques pour le prénom et nom." << std::endl;
        std::cout << "Donne-moi ton prénom et ton nom : ";
        std::getline(std::cin, prenomNom);
    }

    bool estNom = false;

    // parcourir chaque caractère de prenomNom
    for (int i = 0; i < prenomNom.length(); i++)
    {
        // mettre en majuscule pour la première lettre du prénom ou pour toutes les lettres du nom
        if (i == 0 || estNom)
        {
            prenomNom[i] = std::toupper(prenomNom[i]);
        }
        // au premier espace rencontré on considère que tout ce qu'il y a après c'est le nom
        else if (prenomNom[i] == ' ' && !estNom)
        {
            estNom = true;
        }
        // sinon mettre en minuscule
        else
        {
            prenomNom[i] = std::tolower(prenomNom[i]);
        }
    }

    std::cout << "Salut " << prenomNom << " !" << std::endl;
}

// III.2.1 & 2.2
void devinerNombre()
{
    srand(time(0));
    int nombreADeviner = rand() % 1001;
    bool nombrePasTrouve = true;
    int nombreEssais = 0;

    while (nombrePasTrouve)
    {
        nombreEssais++;
        std::string message = "Devine le nombre auquel je pense (se situe entre 0 et 1000) : ";

        std::cout << message;
        int nombre;
        std::cin >> nombre;

        // gestion des erreurs
        while (std::cin.fail() || nombre < 0 || nombre > 1000)
        {
            std::cout << "Erreur : merci de saisir un nombre entre 0 et 1000." << std::endl;
            std::cout << message;
            std::cin.clear();
            std::cin.ignore(256, '\n');
            std::cin >> nombre;
        }

        if (nombre > nombreADeviner)
        {
            std::cout << "Le nombre que tu as saisi est plus grand que le nombre auquel je pense !" << std::endl;
        }
        else if (nombre < nombreADeviner)
        {
            std::cout << "Le nombre que tu as saisi est plus petit que le nombre auquel je pense !" << std::endl;
        }
        else
        {
            std::cout << "Bingo ! C'est exactement le nombre auquel je pensais. Nombre d'essais effectués : " << nombreEssais << std::endl;
            nombrePasTrouve = false;
        }
    }
}

void afficherMessageVictoire(int nombreMystere, int nombreEssais)
{
    std::cout << "Youhouuu ! J'ai trouvé ton nombre mystère qui était " << nombreMystere << ". Nombre d'essais effectués : " << nombreEssais << std::endl;
}

// III.2.3
void devinerNombreOrdinateur()
{
    int a = 0;
    int b = 1000;
    int nombreADeviner;
    bool nombrePasTrouve = true;
    int nombreEssais = 0;

    while (nombrePasTrouve)
    {
        nombreEssais++;
        nombreADeviner = (a + b) / 2;

        std::cout << "Est-ce le nombre " << nombreADeviner << " ? ";
        std::string reponse;
        std::cin >> reponse;

        // gestion des erreurs
        while (std::cin.fail() || reponse != "+" && reponse != "-" && reponse != "Y" && reponse != "y")
        {
            std::cout << "Erreur : merci de saisir une réponse valide entre '+' (nombre plus grand), '-' (nombre plus petit) ou 'Y' (nombre juste)." << std::endl;
            std::cout << "Est-ce le nombre " << nombreADeviner << " ? ";
            std::cin.clear();
            std::cin.ignore(256, '\n');
            std::cin >> reponse;
        }

        if (reponse == "+")
        {
            a = nombreADeviner;

            // il n'y a plus que 1000 comme nombre, on affiche tout de suite le message
            if (a == 999)
            {
                afficherMessageVictoire(1000, nombreEssais);
                break;
            }
        }
        else if (reponse == "-")
        {
            b = nombreADeviner;

            // il n'y a plus que 0 comme nombre, on affiche tout de suite le message
            if (b == 1)
            {
                afficherMessageVictoire(0, nombreEssais);
                break;
            }
        }
        else
        {
            afficherMessageVictoire(nombreADeviner, nombreEssais);
            nombrePasTrouve = false;
        }

        // Si on est entre deux nombres proches, on déduit que c'est le nombre du milieu (ex: a = 501 et b = 503, alors nombreMystere = 502)
        if (b - a == 2 && a != 998)
        {
            nombreADeviner = (a + b) / 2;
            afficherMessageVictoire(nombreADeviner, nombreEssais);
            nombrePasTrouve = false;
        }
    }
}