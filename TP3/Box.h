#ifndef BOX_H
#define BOX_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

// Classe d'une case
class Box
{
public:
	/**
	 * Constructeur
	**/
	Box();

	/**
	 * Getters
	**/
	inline int GetIdBox() const { return idBox; }
	inline int GetOwner() const { return owner; }

	/**
	 * Setters
	**/
	inline void SetOwner(const int& NewOwner) { owner = NewOwner; }
	inline void static SetIdStatic(const int& NewIdStatic) { idStatic = NewIdStatic; }

	/**
	 * M�thodes
	**/
	string DisplayBoxTicTacToe() const;
	string DisplayBoxPowerFour() const;

private:
	/**
	 * Attributs
	**/
	static int idStatic;
	int idBox;
	int owner;
};

#endif