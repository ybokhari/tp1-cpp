#ifndef MANIP_NOMBRES_H
#define MANIP_NOMBRES_H

// I.1.1
int Somme(int a, int b);

// I.1.2
void Inverser(int &a, int &b);

// I.1.3
void RemplacerParam3AvecPnt(int a, int b, int *c);
void RemplacerParam3AvecRef(int d, int e, int &f);

// I.1.4
void GenererTableau(int taille, int tri);

#endif // MANIP_NOMBRES_H