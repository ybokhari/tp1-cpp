#include "TicTacToe.h"

/**
 * @brief   Construit un nouveau jeu de morpion
**/
TicTacToe::TicTacToe()
{
	InitGrid(3, 3);
	player1 = Player(1, "1");
	player2 = Player(2, "2");
}

/**
 * @brief   Obtenir la position d'une case dans la grille
 * @param  idBox : id de la case
 * @return  la case si elle est trouv�e, une case vide sinon
**/
Box& TicTacToe::GetBoxPosition(const int& idBox)
{
	switch (idBox)
	{
	case 0:
		return grid[0][0];
	case 1:
		return grid[0][1];
	case 2:
		return grid[0][2];
	case 3:
		return grid[1][0];
	case 4:
		return grid[1][1];
	case 5:
		return grid[1][2];
	case 6:
		return grid[2][0];
	case 7:
		return grid[2][1];
	case 8:
		return grid[2][2];
	default:
		Box box;
		return box;
	}
}

/**
 * @brief   Initialise une grille de jeu
 * @params  nbLines : nombre de lignes, nbColumns : nombre de colonnes
 * @return  void
**/
void TicTacToe::InitGrid(const int& nbLines, const int& nbColumns)
{
	grid.resize(nbLines);
	for (int i = 0; i < nbLines; i++) {
		grid[i].resize(nbColumns);
	}
	Box::SetIdStatic(1);
}

/**
 * @brief   Affiche une grille de jeu
 * @return  void
**/
void TicTacToe::DisplayGrid() const
{
	cout << "Joueur 1 (X) - Joueur 2 (O)" << endl << endl;

	string line;
	for (int i = 0; i < 3; i++)
	{
		line = "";
		for (int j = 0; j < 3; j++)
		{
			line += grid[i][j].DisplayBoxTicTacToe();
			if (j < 2)
			{
				line += " - ";
			}
		}
		cout << line << endl;
		if (i < 2)
		{
			cout << "|   |   |" << endl;
		}
	}

	cout << endl;
}

/**
 * @brief   Demande une entr�e au joueur
 * @param   player : joueur
 * @return  void
**/
void TicTacToe::InputPlayer(Player player)
{
	int input = 0;
	cin >> input;

	// v�rifier si l'entr�e est valide (nombre compris entre 1 et 9)
	while (!std::cin.good() || input < 1 || input > 9)
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cout << "Veuillez saisir un nombre entre 1 et 9 : ";
		cin >> input;
	}

	// v�rifier si la case est libre
	if (GetBoxPosition(input - 1).GetOwner() == 0)
	{
		GetBoxPosition(input - 1).SetOwner(player.GetId());
	}
	else
	{
		cout << "Case deja prise !" << endl;
		cout << "Veuillez saisir un autre nombre : ";
		InputPlayer(player);
	}
}

/**
 * @brief   Lance un tour de jeu
 * @return  void
**/
void TicTacToe::PlayRound()
{
	int i = 0;
	Player currentPlayer = player1;

	while (!PlayerHasWon(currentPlayer) && !NobodyWon())
	{
		if (i % 2 == 0)
		{
			currentPlayer = player1;
		}
		else
		{
			currentPlayer = player2;
		}

		system("cls");
		DisplayGrid();
		cout << "Au tour du joueur " << currentPlayer.GetName() << ", ou souhaitez-vous jouer ? ";
		InputPlayer(currentPlayer);
		i++;
	}

	system("cls");
	DisplayGrid();

	if (PlayerHasWon(currentPlayer))
	{
		cout << "Victoire pour le joueur " << currentPlayer.GetName() << " !" << endl;
	}
	else if (NobodyWon())
	{
		cout << "Aucun joueur n'a remporte la victoire" << endl;
	}
}

/**
 * @brief   V�rifie si aucun joueur n'a gagn� � l'issue du jeu
 * @return  true si aucun joueur n'a gagn�, false sinon
**/
bool TicTacToe::NobodyWon() const
{
	int line = 0;
	int column = 0;
	for (line = 0; line < grid.size(); line++)
	{
		for (column = 0; column < grid[line].size(); column++)
		{
			if (grid[line][column].GetOwner() == 0)
			{
				return false;
			}
		}
	}
	return true;
}

/**
 * @brief   V�rifie si un joueur a gagn�
 * @param   player : joueur
 * @return  true si le joueur a gagn�, false sinon
**/
bool TicTacToe::PlayerHasWon(const Player& player) const
{
	return PlayerHasWonByLine(player) || PlayerHasWonByColumn(player) || PlayerHasWonByDiagonal(player);
}

/**
 * @brief   V�rifie si un joueur a gagn� en ligne
 * @param   player : joueur
 * @return  true si le joueur a gagn� en ligne, false sinon
**/
bool TicTacToe::PlayerHasWonByLine(const Player& player) const
{
	int count = 0;
	for (int line = 0; line < grid.size(); line++)
	{
		for (int column = 0; column < grid[line].size(); column++)
		{
			if (grid[line][column].GetOwner() == player.GetId())
			{
				count++;
			}
		}
		if (count == 3)
		{
			return true;
		}
		count = 0;
	}
	return false;
}

/**
 * @brief   V�rifie si un joueur a gagn� en colonne
 * @param   player : joueur
 * @return  true si le joueur a gagn� en colonne, false sinon
**/
bool TicTacToe::PlayerHasWonByColumn(const Player& player) const
{
	int count = 0;
	for (int column = 0; column < grid[0].size(); column++) {

		for (int line = 0; line < grid.size(); line++)
		{
			if (grid[line][column].GetOwner() == player.GetId())
			{
				count++;
			}
		}
		if (count == 3)
		{
			return true;
		}
		count = 0;
	}
	return false;
}

/**
 * @brief   V�rifie si un joueur a gagn� en diagonale
 * @param   player : joueur
 * @return  true si le joueur a gagn� en diagonale, false sinon
**/
bool TicTacToe::PlayerHasWonByDiagonal(const Player& player) const
{
	int count = 0;
	int count2 = 0;
	for (int i = 0; i < 3; i++)
	{
		if (grid[i][i].GetOwner() == player.GetId()) {
			count++;
		}
		if (grid[i][2 - i].GetOwner() == player.GetId()) {
			count2++;
		}
	}
	return count == 3 || count2 == 3;
}