#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include "Box.h"

// Classe m�re des diff�rents jeux
class Game
{
public:
	/**
	 * Constructeur
	**/
	Game();

	/**
	 * M�thodes
	**/
	virtual void InitGrid(const int& nbLines, const int& nbColumns) = 0;
	virtual void DisplayGrid() const = 0;
	virtual void InputPlayer(Player player) = 0;
	virtual void PlayRound() = 0;
	virtual bool NobodyWon() const = 0;
	virtual bool PlayerHasWon(const Player& player) const = 0;
	virtual bool PlayerHasWonByLine(const Player& player) const = 0;
	virtual bool PlayerHasWonByColumn(const Player& player) const = 0;
	virtual bool PlayerHasWonByDiagonal(const Player& player) const = 0;

private:
	/**
	 * Attributs
	**/
	vector<vector<Box>> grid;
	Player player1;
	Player player2;
};

#endif